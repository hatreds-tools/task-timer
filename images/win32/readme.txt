Use png2ico to compose icon:
  png2ico clock.ico clock016.png clock024.png clock032.png clock048.png clock064.png clock256.png

App site: http://www.winterdrache.de/freeware/png2ico/

Or use ImageMagick:
  convert *.png clock.ico
