#!/bin/sh

# Prepare small sized images
for s in 48 40 32 24 20 16 10 8
do
    suf=`printf "%.3d" $s`
    convert clock256.png -resize ${s}x${s} clock$suf.png
done

# Compose ICO
convert *.png clock.ico
