#-------------------------------------------------
#
# Project created by QtCreator 2010-11-10T01:37:08
#
#-------------------------------------------------

QT        += core gui widgets svg

TARGET     = task-timer
TEMPLATE   = app

#CONFIG += c++11 c++-0x

QMAKE_CXXFLAGS += -Wall -Wextra -pedantic -std=c++11
#-std=c++14

INCLUDEPATH += src

unix:LIBS += -lXss
win32:RC_FILE = task-timer.rc

SOURCES += \
    src/tasktimerapp.cpp \
    src/main.cpp \
    src/settings.cpp \
    src/menuitem.cpp \
    src/addtask.cpp \
    src/extlabel.cpp \
    src/tracker.cpp \
    src/taskspenttime.cpp \
    src/settingsdialog.cpp \
    src/idledetector.cpp

HEADERS  += \
    src/tasktimerapp.h \
    src/version.h \
    src/settings.h \
    src/menuitem.h \
    src/addtask.h \
    src/extlabel.h \
    src/taskinfo.h \
    src/tracker.h \
    src/taskspenttime.h \
    src/settingsdialog.h \
    src/idledetector.h

RESOURCES += \
    main.qrc

FORMS += \
    src/menuitem.ui \
    src/taskeditor.ui \
    src/taskspenttime.ui \
    src/settingsdialog.ui
