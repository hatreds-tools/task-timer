#include <QPalette>

#include "taskspenttime.h"
#include "ui_taskspenttime.h"

TaskSpentTime::TaskSpentTime(QWidget *parent)
    : TaskSpentTime(nullptr, QString(), parent)
{
}

TaskSpentTime::TaskSpentTime(QHash<QString, TaskInfo> *taskInfo, const QString &id, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::TaskSpentTime),
      m_taskInfo(taskInfo)
{
    ui->setupUi(this);
    setupTasks(id);
}

TaskSpentTime::~TaskSpentTime()
{
    delete ui;
}

void TaskSpentTime::changeEvent(QEvent *e)
{
    QDialog::changeEvent(e);
    switch (e->type()) {
        case QEvent::LanguageChange:
            ui->retranslateUi(this);
            break;
        default:
            break;
    }
}

void TaskSpentTime::setupTasks(const QString &id)
{
    if (!m_taskInfo)
        return;

    auto keys = m_taskInfo->keys();
    qSort(keys.begin(), keys.end(), [](const QString& a, const QString& b) {
        return a > b;
    });

    ui->tasks->clear();

    int counter = 0;
    int idx = -1;
    for (const auto& key : keys)
    {
        if (!id.isEmpty() && id == key)
            idx = counter;
        counter++;
        QString title = (*m_taskInfo)[key].id;
        if (!(*m_taskInfo)[key].title.isEmpty())
            title = (*m_taskInfo)[key].title;
        ui->tasks->addItem(title, key);
    }

    ui->tasks->setCurrentIndex(idx);
}

void TaskSpentTime::setupTimes(const QString &id)
{
    if (!m_taskInfo)
        return;

    qDebug() << "Setup times for task " << id;

    ui->spentTimeTable->setRowCount(0);

    const auto &taskInfo = (*m_taskInfo)[id];
    const auto &timeList = taskInfo.timeList;

    if (timeList.empty())
        return;

    auto keys = timeList.keys();
    qSort(keys.begin(), keys.end(), [](const QDate &a, const QDate& b) {
        return a > b;
    });

    int row = 0;

    const QColor oddCellColor = ui->spentTimeTable->palette().color(QPalette::Base);
    const QColor evenCellColor = ui->spentTimeTable->palette().color(QPalette::AlternateBase);

    QDate monday;
    QDate sunday;

    QColor cellBg = oddCellColor;

    size_t count = 1;

    for (const auto &key : keys)
    {
        qDebug() << "Key: " << key << timeList[key];

        if (timeList[key].time == 0)
            continue;

        auto date = new QTableWidgetItem(key.toString("yyyy-MM-dd, ddd"));
        auto time = new QTableWidgetItem(QString("%1:%2")
                                         .arg(timeList[key].time/60)
                                         .arg(timeList[key].time%60, 2, 10, QLatin1Char('0')));

        if (monday.isNull())
            monday = key.addDays(-key.dayOfWeek() + 1);

        if (sunday.isNull())
            sunday = key.addDays(7 - key.dayOfWeek() - 1);

        if (monday.daysTo(key) < 0 || key.daysTo(sunday) > 7)
        {
            monday = key.addDays(-key.dayOfWeek() + 1);
            sunday = key.addDays(7 - key.dayOfWeek() - 1);
            count++;
        }

        if (count % 2)
        {
            cellBg = oddCellColor;
        }
        else
        {
            cellBg = evenCellColor;
        }

        date->setBackgroundColor(cellBg);
        time->setBackgroundColor(cellBg);

        // Israel, sorry :-)
        if (key.dayOfWeek() == 6 ||
            key.dayOfWeek() == 7)
        {
            date->setTextColor(Qt::red);
            time->setTextColor(Qt::red);
        }

        ui->spentTimeTable->setRowCount(++row);
        ui->spentTimeTable->setItem(row - 1, 0, date);
        ui->spentTimeTable->setItem(row - 1, 1, time);
    }
}

void TaskSpentTime::on_tasks_currentIndexChanged(int index)
{
    qDebug() << "Current index changed: " << index;

    if (index == -1)
        return;

    setupTimes(ui->tasks->itemData(index).toString());
}

void TaskSpentTime::on_goButton_clicked()
{
    if (ui->tasks->currentIndex() == -1)
        return;
    emit goTask(ui->tasks->itemData(ui->tasks->currentIndex()).toString());
}
