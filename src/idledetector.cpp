#include <QDebug>

#include "idledetector.h"

using namespace std;


///
// Platform-specific
#ifdef Q_WS_X11
#include <QX11Info>
#include <X11/extensions/scrnsaver.h>
uint64_t system_timeout()
{
    auto info = XScreenSaverInfo();
    XScreenSaverQueryInfo(QX11Info::display(), QX11Info::appRootWindow(), &info);
    return info.idle / 1000;
}
#elif defined(_WIN32)
//static_assert(0, "Implement support");
// Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ms646272%28v=vs.85%29.aspx
// Ref: https://msdn.microsoft.com/en-us/library/windows/desktop/ms646302%28v=vs.85%29.aspx
#include "windows.h"
uint64_t system_timeout()
{
    auto info = LASTINPUTINFO();
    info.cbSize = sizeof(info);
    GetLastInputInfo(&info);
    return static_cast<uint64_t>((GetTickCount() - info.dwTime) / 1000);
}
#else
#warning User idle timeout does not implemented yet
uint64_t system_timeout()
{
    return 0;
}
#endif
///

IdleDetector::IdleDetector(QObject *parent)
    : QObject(parent)
{
    connect(&m_timer, SIGNAL(timeout()),
            this, SLOT(onTimeout()));
}

void IdleDetector::start()
{
    if (!m_timer.isActive())
        m_timer.start(1000);
}

void IdleDetector::stop()
{
    m_timer.stop();
}

void IdleDetector::setTimeout(int timeout)
{
    qDebug() << "new timeout" << timeout;
    m_timeout = timeout;
}

void IdleDetector::onTimeout()
{
    auto idlesecs = system_timeout();

    //qDebug() << "idle timeout" << idlesecs;

    if (m_timeout)
    {
        if (idlesecs > m_timeout)
        {
            if (!m_isIdle)
            {
                m_isIdle = true;
                emit idle();
            }
        }
        else
        {
            if (m_isIdle)
            {
                m_isIdle = false;
                emit active();
            }
        }
    }
}
