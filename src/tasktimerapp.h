/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Main application class definition.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#ifndef MOUNTTRAYAPP_H
#define MOUNTTRAYAPP_H

#include <memory>

#include <QApplication>
#include <QSystemTrayIcon>
#include <QWidgetAction>
#include <QMessageBox>
#include <QMenu>
#include <QHash>
#include <QTimer>
#include <QSignalMapper>

#include "menuitem.h"
#include "taskinfo.h"
#include "idledetector.h"

class TaskTimerApp : public QApplication
{
    Q_OBJECT

public:
    explicit TaskTimerApp(int &argc, char **argv);

#ifdef _WIN32
    void processGlobalMouseClick();
#endif

private slots:
    void onTaskBrowse(const QString &id);
    void onTaskActivated(const QString &id);
    void onTaskRemove(const QString &id);
    void onTaskEdit(const QString &id);
    void onTaskKeepTime(QObject *action);
    void onTaskResetTime(const QString &id);
    void onTaskRequestTime(const QString &id);
    void onTrayActivated(QSystemTrayIcon::ActivationReason reason);
    void onAbout();
    void onReset();
    void onSettings();
    void onTimer();
    void onDayTimer();
    void onUserIdle();
    void onUserActive();
    void onPauseResume();
    void onTaskAdd();
    void onHide();

private:
    enum TimerState {
        TimerStop,
        TimerActive,
        TimerPause
    };

private:
    void initTasks();
    void addMenuItem(const QString &taskId, TaskInfo &info);
    void removeMenuItem(const QString &taskId);
    void updateMenuItemText(const QString &taskId, const QString &title, const QString &description);
    void updateMenuItemTime(const QString &taskId, const TaskInfo &info);
    void updateMenuItemState(const QString &taskId, TimerState state);

    void showMessage(const QString &text);
    void showError(const QString &text);

    void timerStart();
    void timerStop();
    
    void addEditTask(const QString &taskId = QString());    
    void adjustSize();

private:
    std::unique_ptr<QMenu>           m_mainMenu;
    std::unique_ptr<QMenu>           m_taskMenu;
    std::unique_ptr<QSystemTrayIcon> m_trayIcon;

    QHash<QString, TaskInfo>        m_taskInfo;

    QString                         m_currentTaskId;
    quint32                         m_currentTaskAdjust = 0;
    TimerState                      m_timerState = TimerStop;
    std::unique_ptr<QMessageBox>    m_timeAdjustDialog;

    QTimer                         *m_timer = nullptr;
    QTimer                         *m_dayTimer = nullptr;
    QDate                           m_prevDate = QDate::currentDate();
    IdleDetector                    m_idle;

    QAction                        *m_emptyAction;
    QAction                        *m_addTaskAction;
    QAction                        *m_startStopAllAction;

    QSignalMapper                  *m_taskRemoveMapper;
    QSignalMapper                  *m_taskEditMapper;
    QSignalMapper                  *m_keepTimerMapper;
    QSignalMapper                  *m_resetTimerMapper;
};

#endif // MOUNTTRAYAPP_H
