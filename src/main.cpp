/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Main file. Programm entry point here.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#include <QApplication>

#include "taskinfo.h"
#include "tasktimerapp.h"

int main(int argc, char *argv[])
{
    qRegisterMetaTypeStreamOperators<TimeInfo>("TimeInfo");
    qRegisterMetaTypeStreamOperators<TimeInfoMap>("TimeInfoMap");

    TaskTimerApp app(argc, argv);
    return app.exec();
}
