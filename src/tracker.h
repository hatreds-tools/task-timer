#ifndef ABSTRACTTRACKER_H
#define ABSTRACTTRACKER_H

#include <Qt>
#include <QProcess>

#include "taskinfo.h"

struct TrackerTypeInfo
{
    QString name;
    QString executable;
};

class TrackerType
{
public:
    TrackerType(const QString &name);
    TrackerType(const TrackerTypeInfo &info);

    QString name() const;
    QString executable() const;

    void rename(const QString &name);
    void setExecutable(const QString &executable);

private:
    QString m_name;
};

struct TrackerInfo
{
    QString name;
    QString trackerTypeName;
    QString baseUrl;
    QString login;
    QString password;
};

class Tracker : public QObject
{
    Q_OBJECT
public:
    /**
     * Creates new instance of the tracker. Load parameters from the settings
     * @param name    tracker name (like 'Rhonda Jira')
     * @param parent  pointer to the parent
     */
    Tracker(const QString &name, QObject *parent = nullptr);

    /**
     * Creates new tracker. Fill new settings or overrite existings
     * @param info
     * @param parent
     */
    Tracker(const TrackerInfo &info, QObject *parent = nullptr);
    virtual ~Tracker();

    bool startProgress(const TaskInfo &task);
    bool stopProgress(const TaskInfo &task);
    bool logTime(const TaskInfo &task);
    bool browse(const TaskInfo &task);

    QString name()     const;
    QString trackerTypeName()     const;
    QString baseUrl()  const;
    QString login()    const;
    QString password() const;
    TrackerInfo trackerInfo() const;

    void rename(const QString &newName);
    void setTrackerTypeName(const QString &type);
    void setBaseUrl(const QString &url);
    void setLogin(const QString &login);
    void setPassword(const QString &password);
    void setTrackerInfo(const TrackerInfo &info);

    bool isActive() const;
    void terminate() const;

signals:
    void done(int error, QString text);

private slots:
    void onError(QProcess::ProcessError error);
    void onFinished(int exitCode, QProcess::ExitStatus exitStatus);
    void onReadyReadStdError();
    void onReadyReadStdOutput();

private:
    QString  m_name;
    QProcess m_proc;
};


#endif // ABSTRACTTRACKER_H
