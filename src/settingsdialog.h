#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>

#include "settings.h"

namespace Ui {
class SettingsDialog;
}

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDialog(Settings &settings, QWidget *parent = 0);
    ~SettingsDialog();

private:
    void save();

private slots:
    void on_dialogButtonBox_accepted();

private:
    Ui::SettingsDialog *m_ui;
    Settings           &m_settings;
};

#endif // SETTINGSDIALOG_H
