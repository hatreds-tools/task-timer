/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Ext Label - simple extension for the QLabel that implemented clicked() signal. Implementation.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#include "extlabel.h"

ExtLabel::ExtLabel(QWidget *parent) :
    QLabel(parent)
{
}

void ExtLabel::mousePressEvent(QMouseEvent *ev)
{
    m_clickPoint = ev->pos();
}

void ExtLabel::mouseReleaseEvent(QMouseEvent *ev)
{
    if (m_clickPoint.isNull() == false && ev->pos() == m_clickPoint)
    {
        emit clicked();
    }

    m_clickPoint = QPoint();
}
