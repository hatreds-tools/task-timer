/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Add/Edit task functionality.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#ifndef ADDTASK_H
#define ADDTASK_H

#include <QDialog>

namespace Ui {
class TaskEditor;
}

class TaskEditor : public QDialog
{
    Q_OBJECT

public:
    enum
    {
        AcceptedAndStart = 2
    };

    enum Type
    {
        Add,
        Modify
    };

public:
    explicit TaskEditor(QWidget *parent = nullptr);
    explicit TaskEditor(Type type, QWidget *parent = nullptr);
    ~TaskEditor();

    QString getTaskId() const;
    QString getTaskTitle() const;
    QString getTaskDescription() const;

    void setTaskId(const QString& taskId);
    void setTaskTitle(const QString& taskTitle);
    void setTaskDescription(const QString& taskDescription);

    void setType(Type type);

private slots:
    void on_addStartButton_clicked();
    void on_additionalInfo_toggled(bool arg);

private:
    Ui::TaskEditor *ui;
};

#endif // ADDTASK_H
