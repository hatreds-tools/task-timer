#include <QPushButton>

#include "settingsdialog.h"
#include "ui_settingsdialog.h"

SettingsDialog::SettingsDialog(Settings &settings, QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::SettingsDialog),
    m_settings(settings)
{
    m_ui->setupUi(this);

    if (m_settings->contains("/url/task_url"))
    {
        m_ui->trackerUrl->setText(m_settings->value("/url/task_url").toString());
    }

    if (m_settings->contains("/core/timeout"))
    {
        m_ui->timeoutSpin->setValue(m_settings->value("/core/timeout").toInt());
    }
}

SettingsDialog::~SettingsDialog()
{
    delete m_ui;
}

void SettingsDialog::save()
{
    m_settings->setValue("/url/task_url", m_ui->trackerUrl->text());
    m_settings->setValue("/core/timeout", m_ui->timeoutSpin->value());
}

void SettingsDialog::on_dialogButtonBox_accepted()
{
    save();
    accept();
}
