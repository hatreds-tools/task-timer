/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Simple application settings wrapper definition.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#ifndef SETTINGS_H
#define SETTINGS_H

#include <QSettings>

class Settings
{
public:
    Settings();

    QSettings* operator->();

    operator QSettings&();

private:
    QSettings m_settings;
};

#endif // SETTINGS_H
