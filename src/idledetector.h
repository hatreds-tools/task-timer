#ifndef IDLEDETECTOR_H
#define IDLEDETECTOR_H

#include <QObject>
#include <QTimer>

//class IdleDetectorPriv;

class IdleDetector : public QObject
{
    Q_OBJECT
public:
    explicit IdleDetector(QObject *parent = 0);

    void start();
    void stop();

    void setTimeout(int timeout);

signals:
    void idle();
    void active();

public slots:
    void onTimeout();

private:
    bool     m_isIdle = false;
    uint64_t m_timeout = 0;
    QTimer   m_timer;
};

#endif // IDLEDETECTOR_H
