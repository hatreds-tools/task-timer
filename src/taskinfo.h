#ifndef TASKINFO_H
#define TASKINFO_H

#include <Qt>
#include <QWidgetAction>
#include <QMap>
#include <QDate>
#include <QDebug>

struct TimeInfo
{
    quint32   time;
    bool      logged;
};

using TimeInfoMap = QMap<QDate, TimeInfo>;

struct TaskInfo
{
    QString        id;
    QString        title;
    QString        description;
    quint32        time = 0;
    QString        trackerId;
    TimeInfoMap    timeList;
    bool           hold = false;
    QWidgetAction *action = nullptr;
};

Q_DECLARE_METATYPE(TimeInfo)
Q_DECLARE_METATYPE(TimeInfoMap)

inline
QDataStream& operator<<(QDataStream& os, const TimeInfo &ti)
{
    os << ti.time << ti.logged;
    return os;
}

inline
QDataStream& operator>>(QDataStream& is, TimeInfo &ti)
{
    is >> ti.time >> ti.logged;
    return is;
}

inline
QDebug operator<<(QDebug dbg, const TimeInfo &ti)
{
    dbg << QString("TaskTime[tm=%1, log=%2]").arg(ti.time).arg(ti.logged).toLatin1();
    return dbg;
}

#endif // TASKINFO_H

