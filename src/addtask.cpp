/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Add/Edit task functionality.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#include "addtask.h"
#include "ui_taskeditor.h"

TaskEditor::TaskEditor(QWidget *parent)
    : TaskEditor(Add, parent)
{
}

TaskEditor::TaskEditor(TaskEditor::Type type, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::TaskEditor)
{
    ui->setupUi(this);
    ui->additionalSettingsHolder->setVisible(false);
    setType(type);
}

TaskEditor::~TaskEditor()
{
    delete ui;
}

QString TaskEditor::getTaskId() const
{
    return ui->taskId->text();
}

QString TaskEditor::getTaskTitle() const
{
    return ui->taskTitle->text();
}

QString TaskEditor::getTaskDescription() const
{
    return ui->description->text();
}

void TaskEditor::setTaskId(const QString &taskId)
{
    ui->taskId->setText(taskId);
}

void TaskEditor::setTaskTitle(const QString &taskTitle)
{
    ui->taskTitle->setText(taskTitle);
}

void TaskEditor::setTaskDescription(const QString &taskDescription)
{
    ui->description->setText(taskDescription);
}

void TaskEditor::setType(TaskEditor::Type type)
{
    switch (type)
    {
        case TaskEditor::Add:
            ui->addButton->setText(tr("Add"));
            ui->addStartButton->setText(tr("Add & Start"));
            setWindowTitle(tr("Add task"));
            break;
        case TaskEditor::Modify:
            ui->addButton->setText(tr("Save"));
            ui->addStartButton->setText(tr("Save & Start"));
            setWindowTitle(tr("Edit task"));
            break;
    }
}

void TaskEditor::on_addStartButton_clicked()
{
    done(AcceptedAndStart);
}

void TaskEditor::on_additionalInfo_toggled(bool arg)
{
    ui->additionalSettingsHolder->setVisible(arg);
}
