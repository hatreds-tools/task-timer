/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Task Menu entry implementation.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#include <QDebug>

#include <iostream>

#include "menuitem.h"

MenuItem::MenuItem(QWidget *parent)
    : MenuItem(QString(), QString(), parent)
{
}

MenuItem::MenuItem(const QString &id, const QString &name, QWidget *parent)
    : QWidget(parent),
      m_id(id),
      m_name(name)
{
    setupUi(this);

    setIcon(QIcon(":/ui/no-cancel-stop"));
    setActionIcon(QIcon(":/ui/go"));
    setLabel(name);
    setLabelExt();
    setDescription();

    this->action->setPopupMode(QToolButton::DelayedPopup);
}

void MenuItem::setId(const QString &text)
{
    m_id = text;
}

void MenuItem::changeEvent(QEvent *e)
{
    QWidget::changeEvent(e);
    switch (e->type())
    {
        case QEvent::LanguageChange:
            retranslateUi(this);
            break;
        default:
            break;
    }
}

void MenuItem::setLabel(const QString &text)
{
    QString labelText = text;
    if (labelText.isEmpty())
    {
        labelText = m_id;
    }

    labelText = QString("<a href=\"%1\">%1</a>").arg(labelText);
    this->label->setText(labelText);
}

void MenuItem::setLabelExt(const QString &text, const QString &tooltip)
{
    this->labelExt->setText(text);
    this->labelExt->setToolTip(tooltip);
    if (text.isEmpty())
        this->labelExt->hide();
    else
        this->labelExt->show();
}

void MenuItem::setDescription(const QString &text)
{
    this->description->setText(text);
    if (text.isEmpty())
        this->description->hide();
    else
        this->description->show();
}

void MenuItem::setActionIcon(const QIcon &icon)
{
    m_actionIcon = icon;
    this->action->setIcon(icon);
}

void MenuItem::setIcon(const QIcon &icon)
{
    m_icon = icon;
    this->icon->setPixmap(icon.pixmap(this->icon->geometry().size()));
}

void MenuItem::addCustomAction(QAction *action)
{
    if (m_customActionsMenu == nullptr)
    {
        m_customActionsMenu = new QMenu(this->action);

        this->action->setPopupMode(QToolButton::MenuButtonPopup);
        this->action->setMenu(m_customActionsMenu);
        this->action->setContextMenuPolicy(Qt::CustomContextMenu);

        m_customActionsMenu->setParent(this->action);
    }

    m_customActionsMenu->addAction(action);
}

void MenuItem::removeCustomAction(QAction *action)
{
    if (m_customActionsMenu == nullptr)
        return;

    m_customActionsMenu->removeAction(action);
}

void MenuItem::on_action_clicked()
{
    emit actionActivated(m_id);
}

void MenuItem::on_label_linkActivated(const QString &/*link*/)
{
    emit labelActivated(m_id);
}

void MenuItem::on_icon_clicked()
{
    emit labelActivated(m_id);
}

void MenuItem::on_labelExt_clicked()
{
    emit extLabelActivated(m_id);
}
