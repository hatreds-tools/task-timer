#ifndef TASKSPENTTIME_H
#define TASKSPENTTIME_H

#include <memory>

#include <QDialog>

#include "taskinfo.h"

namespace Ui {
class TaskSpentTime;
}

class TaskSpentTime : public QDialog
{
    Q_OBJECT

public:
    explicit TaskSpentTime(QWidget *parent = nullptr);
    explicit TaskSpentTime(QHash<QString, TaskInfo> *taskInfo, const QString &id = QString(), QWidget *parent = nullptr);
    ~TaskSpentTime();

signals:
    void goTask(const QString& key);

protected:
    void changeEvent(QEvent *e);

private:
    void setupTasks(const QString &id);
    void setupTimes(const QString &id);

private slots:
    void on_tasks_currentIndexChanged(int index);
    void on_goButton_clicked();

private:
    Ui::TaskSpentTime        *ui;
    QHash<QString, TaskInfo> *m_taskInfo = nullptr;
};

#endif // TASKSPENTTIME_H
