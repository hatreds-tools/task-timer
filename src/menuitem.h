/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Task Menu entry definition.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/


#ifndef MENUITEM_H
#define MENUITEM_H

#include <QAction>
#include <QMenu>

#include "ui_menuitem.h"

class MenuItem : public QWidget, private Ui::MenuItem
{
    Q_OBJECT

public:
    explicit MenuItem(QWidget *parent = 0);
    explicit MenuItem(const QString &id, const QString &name = QString(), QWidget *parent = 0);

    void     setId(const QString &text);
    void     setLabel(const QString &text);
    void     setLabelExt(const QString &text = QString(), const QString &tooltip = QString());
    void     setDescription(const QString &text = QString());
    void     setActionIcon(const QIcon &icon);
    void     setIcon(const QIcon &icon = QIcon());

    void     addCustomAction(QAction *action);
    void     removeCustomAction(QAction *action);

signals:
    void actionActivated(QString media_dev);
    void labelActivated(QString media_dev);
    void extLabelActivated(QString media_dev);

protected:
    void changeEvent(QEvent *e) override;

private slots:
    void on_action_clicked();
    void on_label_linkActivated(const QString &link);
    void on_icon_clicked();
    void on_labelExt_clicked();

private:
    QString m_id;
    QString m_name;
    QIcon   m_actionIcon;
    QIcon   m_icon;

    QMenu  *m_customActionsMenu = nullptr;
};

#endif // MENUITEM_H
