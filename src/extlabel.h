/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Ext Label - simple extension for the QLabel that implemented clicked() signal. Declaration.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/


#ifndef EXTLABEL_H
#define EXTLABEL_H

#include <QLabel>
#include <QMouseEvent>

class ExtLabel : public QLabel
{
    Q_OBJECT
public:
    explicit ExtLabel(QWidget *parent = 0);

signals:
    void clicked();

protected:
    void mousePressEvent(QMouseEvent *ev);
    void mouseReleaseEvent(QMouseEvent *ev);

private:
    QPoint m_clickPoint;
};

#endif // EXTLABEL_H
