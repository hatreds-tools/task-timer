#include "settings.h"
#include "tracker.h"

TrackerType::TrackerType(const QString &name)
    : m_name(name)
{
}

TrackerType::TrackerType(const TrackerTypeInfo &info)
    : TrackerType(info.name)
{
    Settings s;
    s->beginGroup(QString("TrackerType-%1").arg(info.name));
    s->setValue("name", info.name);
    s->setValue("executable", info.executable);
    s->endGroup();
}

QString TrackerType::name() const
{
    return m_name;
}

QString TrackerType::executable() const
{
    Settings s;
    return s->value(QString("TrackerType-%1/executable").arg(m_name)).toString();
}

void TrackerType::rename(const QString &name)
{
    Settings s;
    TrackerTypeInfo info;
    info.name = name;
    info.executable = s->value(QString("TrackerType-%1/executable").arg(m_name)).toString();
    s->remove(QString("TrackerType-%1").arg(m_name));

    // Create new tracker
    TrackerType type {info};
    std::swap(*this, type);

    // Rename tacker type info
    auto groups = s->childGroups();
    for (QString &v : groups) {
        if (!v.startsWith("Tracker-"))
            continue;
        s->setValue(v + "/type", name);
    }
}

void TrackerType::setExecutable(const QString &executable)
{
    Settings s;
    s->setValue(QString("TrackerType-%1/executable").arg(m_name), executable);
}


Tracker::Tracker(const QString &name, QObject *parent)
    : QObject(parent),
      m_name(name)
{

}

Tracker::~Tracker()
{

}

bool Tracker::startProgress(const TaskInfo &task)
{
    static_cast<void>(task);
    return false;
}

bool Tracker::stopProgress(const TaskInfo &task)
{
    static_cast<void>(task);
    return false;
}

bool Tracker::logTime(const TaskInfo &task)
{
    static_cast<void>(task);
    return false;
}

bool Tracker::browse(const TaskInfo &task)
{
    static_cast<void>(task);
    return false;
}


void Tracker::onError(QProcess::ProcessError error)
{
    static_cast<void>(error);
}

void Tracker::onFinished(int exitCode, QProcess::ExitStatus exitStatus)
{
    static_cast<void>(exitCode);
    static_cast<void>(exitStatus);
}

void Tracker::onReadyReadStdError()
{

}

void Tracker::onReadyReadStdOutput()
{

}
