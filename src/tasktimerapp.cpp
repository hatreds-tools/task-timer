/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Main application class implementation.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/

#include <iostream>

#include <QDebug>
#include <QMessageBox>
#include <QDesktopServices>
#include <QUrl>
#include <QDate>
#include <QPainter>
#include <QTextStream>

#include "tasktimerapp.h"
#include "version.h"
#include "settings.h"
#include "addtask.h"
#include "taskspenttime.h"
#include "settingsdialog.h"

#ifdef _WIN32
#include <windows.h>
//#include <winuser.h>
// HACK, see http://stackoverflow.com/questions/20401896/qt-global-mouse-listener
HHOOK g_hook = nullptr;
LRESULT CALLBACK MouseProc(int code, WPARAM wparam, LPARAM lparam)
{
    switch (wparam)
    {
        case WM_LBUTTONDOWN:
        case WM_RBUTTONDOWN:
        case WM_MBUTTONDOWN:
            auto app = static_cast<TaskTimerApp*>(qApp);
            app->processGlobalMouseClick();
            break;
    }
    return CallNextHookEx(g_hook, code, wparam, lparam);
}
#endif

TaskTimerApp::TaskTimerApp(int & argc, char ** argv) :
    QApplication(argc, argv)
{
    setOrganizationName(QLatin1String("HatredsLogPlace"));
    setOrganizationDomain(QLatin1String("htrd.su"));
    setApplicationName(QLatin1String("TaskTimer"));
    setApplicationVersion(QLatin1String(APP_VERSION_FULL));

    setWindowIcon(QIcon(":/ui/clock"));
    setQuitOnLastWindowClosed(false);

#ifdef _WIN32
    g_hook = SetWindowsHookEx(WH_MOUSE_LL, MouseProc, nullptr, 0);
#endif

    // Display task menu
    m_taskMenu.reset(new QMenu());
    m_addTaskAction = m_taskMenu->addAction(QIcon(":/ui/add"), tr("Add task"), this, SLOT(onTaskAdd()));
    m_taskMenu->addSeparator();
    m_emptyAction = m_taskMenu->addAction(tr("empty"));
    m_taskMenu->addSeparator();
    m_startStopAllAction = m_taskMenu->addAction(QIcon(":/ui/pause"), tr("Pause/Resume all"), this, SLOT(onPauseResume()));

    m_addTaskAction->setIconVisibleInMenu(true);
    m_startStopAllAction->setIconVisibleInMenu(true);

    m_emptyAction->setEnabled(false);
    m_startStopAllAction->setEnabled(false);

    // Exit, config and etc menu
    m_mainMenu.reset(new QMenu());
    m_mainMenu->setWindowFlags(Qt::FramelessWindowHint | Qt::Popup);
    m_mainMenu->addAction(tr("About"), this, SLOT(onAbout()));
    m_mainMenu->addSeparator();
    m_mainMenu->addAction(tr("Reset"), this, SLOT(onReset()));
    m_mainMenu->addAction(tr("Settings..."), this, SLOT(onSettings()));
    m_mainMenu->addSeparator();
    m_mainMenu->addAction(tr("Exit"), this, SLOT(quit()));

    m_trayIcon.reset(new QSystemTrayIcon());
    m_trayIcon->setIcon(QIcon(":/ui/clock"));
    m_trayIcon->show();
    m_trayIcon->setContextMenu(m_mainMenu.get());

    connect(m_trayIcon.get(), SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this,             SLOT(onTrayActivated(QSystemTrayIcon::ActivationReason)));

    m_timer = new QTimer(this);
    m_timer->setInterval(60000); // 60 sec
    connect(m_timer, SIGNAL(timeout()),
            this,     SLOT(onTimer()));

    m_dayTimer = new QTimer(this);
    m_dayTimer->setInterval(10000); // 10 sec
    connect(m_dayTimer, SIGNAL(timeout()),
            this,       SLOT(onDayTimer()));
    m_dayTimer->start(); // work forever

    m_taskRemoveMapper = new QSignalMapper(this);
    connect(m_taskRemoveMapper, SIGNAL(mapped(QString)),
            this,               SLOT(onTaskRemove(QString)));

    m_taskEditMapper = new QSignalMapper(this);
    connect(m_taskEditMapper, SIGNAL(mapped(QString)),
            this,             SLOT(onTaskEdit(QString)));

    m_keepTimerMapper = new QSignalMapper(this);
    connect(m_keepTimerMapper, SIGNAL(mapped(QObject*)),
            this,              SLOT(onTaskKeepTime(QObject*)));

    m_resetTimerMapper = new QSignalMapper(this);
    connect(m_resetTimerMapper, SIGNAL(mapped(QString)),
            this,               SLOT(onTaskResetTime(QString)));

    connect(&m_idle, SIGNAL(idle()),
            this, SLOT(onUserIdle()));
    connect(&m_idle, SIGNAL(active()),
            this, SLOT(onUserActive()));

    initTasks();
}

void TaskTimerApp::initTasks()
{
    Settings settings;

    settings->beginGroup("/tasks");
    QStringList taskIds = settings->childGroups();

    qDebug() << "Stored tasks: " << taskIds;

    for (int i = 0; i < taskIds.size(); ++i)
    {
        QString taskId = taskIds[i].toLower();

        TaskInfo info;

        info.id          = settings->value(taskId + "/id").toString();
        info.title       = settings->value(taskId + "/title").toString();
        info.description = settings->value(taskId + "/description").toString();
        info.time        = settings->value(taskId + "/time").toULongLong();
        info.trackerId   = settings->value(taskId + "/tracker_id").toString();
        info.hold        = settings->value(taskId + "/hold").toBool();

        quint32 time = 0;
        auto times = settings->value(taskId + "/track_times");
        if (times.canConvert<TimeInfoMap>()) {
            auto map = times.value<TimeInfoMap>();
            for (auto it = map.begin(); it != map.end(); ++it) {
                QDate    key   = it.key();
                TimeInfo value = it.value();
                time += value.time;
                info.timeList[key] = value;

                qDebug() << key << value;
            }
        }

        if (time == 0) {
            TimeInfo ti = {info.time, false};
            info.timeList[QDate::currentDate()] = ti;
        } else {
            info.time = time;
        }

#if 0
        info.timeList[QDate::fromString("2015-11-04", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-11-04", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-11-29", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-11-29", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-11-30", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-11-30", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-01", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-01", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-02", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-02", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-03", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-03", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-04", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-04", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-05", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-05", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-06", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-06", "yyyy-MM-dd")].time   = 1;
        info.timeList[QDate::fromString("2015-12-07", "yyyy-MM-dd")].logged = false;
        info.timeList[QDate::fromString("2015-12-07", "yyyy-MM-dd")].time   = 1;
#endif

        addMenuItem(taskId, info);
        updateMenuItemTime(taskId, info);
    }
    settings->endGroup();

    if (!settings->contains("/url/task_url"))
    {
        settings->setValue("/url/task_url", "http://jira.localhost.localdomain/browse/{s}");
    }

    if (settings->contains("/core/timeout"))
    {
        m_idle.setTimeout(settings->value("/core/timeout", 0).toInt() * 60);
        m_idle.start();
    }

}

void TaskTimerApp::addMenuItem(const QString &taskId, TaskInfo& info)
{
    QWidgetAction *action = new QWidgetAction(m_trayIcon.get());
    MenuItem      *item   = new MenuItem(info.id, info.title);
    action->setDefaultWidget(item);

    if (m_taskInfo.size() == 0)
    {
        m_taskMenu->removeAction(m_emptyAction);
        m_emptyAction->deleteLater();
        m_emptyAction = 0;
    }

    // Insert on Top
    m_taskMenu->insertAction(m_taskMenu->actions().at(2), action);

    // Save action
    info.action = action;
    m_taskInfo[taskId] = info;

    item->setDescription(info.description);
    updateMenuItemTime(taskId, info);

    // Connect signals
    connect(item, SIGNAL(labelActivated(QString)),
            this, SLOT(onTaskActivated(QString)));
    connect(item, SIGNAL(actionActivated(QString)),
            this, SLOT(onTaskBrowse(QString)));
    connect(item, SIGNAL(extLabelActivated(QString)),
            this, SLOT(onTaskRequestTime(QString)));

    QAction *removeAction = new QAction(QIcon(":/ui/remove"), tr("Remove task"), item);
    removeAction->setIconVisibleInMenu(true);
    item->addCustomAction(removeAction);

    connect(removeAction, SIGNAL(triggered()),
            m_taskRemoveMapper, SLOT(map()));
    m_taskRemoveMapper->setMapping(removeAction, taskId);


    QAction *editAction = new QAction(QIcon(":/ui/edit"), tr("Edit task"), item);
    editAction->setIconVisibleInMenu(true);
    item->addCustomAction(editAction);

    connect(editAction, SIGNAL(triggered()),
            m_taskEditMapper, SLOT(map()));
    m_taskEditMapper->setMapping(editAction, taskId);


    auto holdAction = new QAction(tr("Keep timer"), item);
    holdAction->setCheckable(true);
    holdAction->setChecked(info.hold);
    holdAction->setToolTip(tr("Keep timer value across full reset"));
    holdAction->setProperty("taskid", taskId);
    item->addCustomAction(holdAction);

    connect(holdAction, SIGNAL(triggered()),
            m_keepTimerMapper, SLOT(map()));
    m_keepTimerMapper->setMapping(holdAction, holdAction);


    auto resetAction = new QAction(tr("Reset time"), item);
    resetAction->setIconVisibleInMenu(true);
    item->addCustomAction(resetAction);

    connect(resetAction, SIGNAL(triggered()),
            m_resetTimerMapper, SLOT(map()));
    m_resetTimerMapper->setMapping(resetAction, taskId);

    adjustSize();
}

void TaskTimerApp::removeMenuItem(const QString &taskId)
{
    QWidgetAction *action = 0;
    if (m_taskInfo.contains(taskId.toLower()))
    {
        action = m_taskInfo[taskId.toLower()].action;
        m_taskMenu->removeAction(action);
        m_taskInfo.remove(taskId.toLower());
        action->deleteLater();
    }

    if (m_taskInfo.size() == 0)
    {
        m_currentTaskId.clear();
        timerStop();
        m_emptyAction = new QAction(tr("empty"), this);
        m_emptyAction->setEnabled(false);
        m_taskMenu->insertAction(m_taskMenu->actions().at(2), m_emptyAction);
    }

    adjustSize();
}

void TaskTimerApp::updateMenuItemText(const QString &taskId, const QString &title, const QString &description)
{
    QWidgetAction *action = 0;
    if (m_taskInfo.contains(taskId.toLower()))
    {
        action = m_taskInfo[taskId.toLower()].action;
        MenuItem *item = static_cast<MenuItem*>(action->defaultWidget());

        if (item == 0)
        {
            return;
        }

        if (!title.isEmpty())
        {
            item->setLabel(title);
        }

        item->setDescription(description);
    }

    adjustSize();
}


void TaskTimerApp::updateMenuItemTime(const QString &taskId, const TaskInfo& info)
{
    if (m_taskInfo.contains(taskId.toLower()))
    {
        auto action     = info.action;
        auto item       = static_cast<MenuItem*>(action->defaultWidget());
        auto value      = info.time;
        auto todayValue = 0;

        if (info.timeList.contains(QDate::currentDate()))
        {
            const auto &ti  = info.timeList[QDate::currentDate()];
            todayValue = ti.time;
        }

        if (item == 0)
            return;

        auto h = value / 60;
        auto m = value % 60;

        auto htoday = todayValue / 60;
        auto mtoday = todayValue % 60;

        QString timeStr = QString("<small>%1:%2</small> <big><b>%3:%4</b></big>")
                          .arg(h).arg(m, 2, 10, QLatin1Char('0'))
                          .arg(htoday).arg(mtoday, 2, 10, QLatin1Char('0'));
        QString timeTooltip = QString("<b>Spent time.</b><br/>"
                                      "First value (small) - total time<br/>"
                                      "Second value (big) - today time");
        item->setLabelExt(timeStr, timeTooltip);
    }
}

void TaskTimerApp::updateMenuItemState(const QString &taskId, TaskTimerApp::TimerState state)
{
    QWidgetAction *action = 0;
    if (m_taskInfo.contains(taskId.toLower()))
    {
        action = m_taskInfo[taskId.toLower()].action;
        MenuItem *item = static_cast<MenuItem*>(action->defaultWidget());

        if (item == 0)
        {
            return;
        }

        switch (state)
        {
            case TaskTimerApp::TimerStop:
                item->setIcon(QIcon(":/ui/no-cancel-stop"));
                m_trayIcon->setIcon(QIcon(":/ui/clock"));
                break;
            case TaskTimerApp::TimerActive:
                item->setIcon(QIcon(":/ui/yes-ok"));
                m_trayIcon->setIcon(QIcon(":/ui/clock-play"));
                break;
            case TaskTimerApp::TimerPause:
                item->setIcon(QIcon(":/ui/pause"));
                m_trayIcon->setIcon(QIcon(":/ui/clock-pause"));
                break;
        }

        m_timerState = state;
    }
}

void TaskTimerApp::showMessage(const QString &text)
{
    m_trayIcon->showMessage("TaskTimer", text);
}

void TaskTimerApp::showError(const QString &text)
{
    m_trayIcon->showMessage("TaskTimer Error", text, QSystemTrayIcon::Critical);
}

void TaskTimerApp::timerStart()
{
    if (m_timer->isActive() == false)
    {
        m_timer->start();
        m_startStopAllAction->setIcon(QIcon(":/ui/pause"));
    }

    if (m_currentTaskId.isEmpty() == false)
        m_startStopAllAction->setEnabled(true);
}

void TaskTimerApp::timerStop()
{
    if (m_timer->isActive() == true)
    {
        m_timer->stop();
        m_startStopAllAction->setIcon(QIcon(":/ui/start"));
    }

    if (m_currentTaskId.isEmpty())
        m_startStopAllAction->setEnabled(false);
}

void TaskTimerApp::addEditTask(const QString &taskId)
{
    TaskEditor dlg;
    bool       isEdit = false;

    TaskInfo oldTaskInfo;

    if (taskId.isEmpty() == false)
    {
        isEdit = true;

        if (m_taskInfo.contains(taskId) == false)
            return;

        oldTaskInfo = m_taskInfo[taskId];

        dlg.setTaskId(oldTaskInfo.id);
        dlg.setType(TaskEditor::Modify);

        if (oldTaskInfo.id != oldTaskInfo.title)
            dlg.setTaskTitle(oldTaskInfo.title);

        dlg.setTaskDescription(oldTaskInfo.description);
    }

    int ret = dlg.exec();

    switch (ret)
    {
        case QDialog::Accepted:
        case TaskEditor::AcceptedAndStart:

            QString id    = dlg.getTaskId();
            QString title = dlg.getTaskTitle();

            if (id.isEmpty())
                return;

            QString newTaskId = id.toLower();

            if (title.isEmpty())
                title = id;

            TaskInfo info;

            info.id          = id;
            info.title       = title;
            info.description = dlg.getTaskDescription();

            if (isEdit)
            {
                info.time     = oldTaskInfo.time;
                info.timeList = oldTaskInfo.timeList;
            }

            // Ready to update settings
            Settings s;

            if (isEdit && taskId == newTaskId)
            {
                // only update exists
                updateMenuItemText(newTaskId, title, info.description);
                m_taskInfo[newTaskId].description = info.description;
                m_taskInfo[newTaskId].title       = info.title;
            }
            else
            {
                // recreate
                if (m_taskInfo.contains(newTaskId))
                {
                    showError(tr("Given task already present. Ignore them."));
                    return;
                }

                addMenuItem(newTaskId, info);

                if (isEdit)
                {
                    removeMenuItem(taskId);
                    s->remove("/tasks/" + taskId);
                }
            }

            // Add to settings
            s->setValue("/tasks/" + newTaskId + "/id",          info.id);
            s->setValue("/tasks/" + newTaskId + "/title",       info.title);
            s->setValue("/tasks/" + newTaskId + "/description", info.description);
            s->setValue("/tasks/" + newTaskId + "/time",        info.time);
            s->setValue("/tasks/" + newTaskId + "/track_times", QVariant::fromValue(info.timeList));

            if (ret == TaskEditor::AcceptedAndStart)
            {
                onTaskActivated(newTaskId);
            }

            break;
    }

    adjustSize();
}

void TaskTimerApp::adjustSize()
{
    for (auto &task : m_taskInfo)
    {
        // HACK: it there more clean way to adopt WidgetAction size to the actual one (changed after
        //       creation)?
        task.action->setVisible(false);
        task.action->setVisible(true);
    }
}

#ifdef _WIN32
void TaskTimerApp::processGlobalMouseClick()
{
    if (m_taskMenu->isVisible())
    {
        if (m_taskMenu->rect().contains(m_taskMenu->mapFromGlobal(QCursor::pos())) == false)
        {
            m_taskMenu->close();
        }
    }
}
#endif

/**************************************************************************************************/
/* Signals ---------------------------------------------------------------------------------------*/
/**************************************************************************************************/

void TaskTimerApp::onTaskBrowse(const QString &id)
{
    m_taskMenu->close();

    QString taskId = id.toLower();

    if (m_taskInfo.contains(taskId))
    {
        Settings s;
        QString url = s->value("/url/task_url").toString();
        if (url.isEmpty() == false)
        {
            url.replace("{s}", m_taskInfo[taskId].id);
            QDesktopServices::openUrl(QUrl(url, QUrl::TolerantMode));
        }
    }
}

void TaskTimerApp::onTaskActivated(const QString &id)
{
    m_taskMenu->close();

    QString taskId = id.toLower();

    if (m_currentTaskId.isEmpty() == false)
    {
        updateMenuItemState(m_currentTaskId, TimerStop);
    }

    if (m_currentTaskId == taskId)
    {
        onPauseResume();
    }
    else
    {
        m_currentTaskId = taskId;
        updateMenuItemState(m_currentTaskId, TimerActive);
        timerStart();
    }
}

void TaskTimerApp::onTaskRemove(const QString &id)
{
    m_taskMenu->close();

    QString taskId = id.toLower();

    int ret = QMessageBox::question(0,
                                    tr("Task Remove"),
                                    tr("Do you really want remove task '%1'?").arg(id),
                                    QMessageBox::Yes,
                                    QMessageBox::No);
    if (ret == QMessageBox::Yes)
    {
        if (taskId== m_currentTaskId)
        {
            m_currentTaskId.clear();
            timerStop();
        }

        removeMenuItem(taskId);

        // Update settings
        Settings s;
        s->remove("/tasks/" + taskId);
    }
}

void TaskTimerApp::onTaskEdit(const QString &id)
{
    QString taskId = id.toLower();
    addEditTask(taskId);
}

void TaskTimerApp::onTaskKeepTime(QObject *object)
{
    m_taskMenu->close();

    auto action = qobject_cast<QAction*>(object);
    if (!action || action->property("taskid").isNull())
        return;

    auto taskId = action->property("taskid").toString().toLower();
    if (m_taskInfo.contains(taskId))
    {
        auto &info = m_taskInfo[taskId];
        info.hold = action->isChecked();
        Settings s;
        s->setValue("/tasks/" + taskId + "/hold", info.hold);
    }
}

void TaskTimerApp::onTaskResetTime(const QString &id)
{
    m_taskMenu->close();

    auto taskId = id.toLower();
    if (m_taskInfo.contains(taskId))
    {
        auto &info = m_taskInfo[taskId];

        int ret = QMessageBox::question(0,
                                        tr("Time Reset"),
                                        tr("Do you really want reset spent time for task %1?").arg(id),
                                        QMessageBox::Yes,
                                        QMessageBox::No);
        if (ret == QMessageBox::Yes)
        {
            info.time = 0;
            info.timeList.clear();
            updateMenuItemTime(id, info);

            // Update settings
            Settings s;
            s->setValue("/tasks/" + id + "/time",  info.time);
            s->setValue("/tasks/" + id + "/track_times", QVariant::fromValue(info.timeList));
        }
    }
}

void TaskTimerApp::onTaskRequestTime(const QString &id)
{
    auto taskId = id.toLower();
    TaskSpentTime dlg(&m_taskInfo, taskId);
    connect(&dlg, SIGNAL(goTask(QString)),
            this, SLOT(onTaskBrowse(QString)));
    dlg.exec();
    disconnect(&dlg);
}

void TaskTimerApp::onTrayActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason)
    {
        case QSystemTrayIcon::Trigger:
        {
            if (m_taskMenu->isHidden())
                m_taskMenu->exec(QCursor::pos());
            else
                m_taskMenu->close();
            break;
        }

        case QSystemTrayIcon::MiddleClick:
        {
            onPauseResume();
            break;
        }

        default:
        {
            m_taskMenu->close();
            return;
        }
    }
}

void TaskTimerApp::onAbout()
{
    const static int develop_begin = 2013;
    QDate   date          = QDate::currentDate();
    QString date_text     = QString("%1").arg(develop_begin);

    if (date.year() > develop_begin)
    {
        date_text += QString("-%1").arg(date.year());
    }

    QMessageBox::about(0,
                       tr("About TaskTimer"),
                       tr("<h2>About TaskTimer</h2>"
                          "<p>Version: " APP_VERSION_FULL "</p>"
                          "<p>Application to measure spent time per-task</p>\n"
                          "<p>This programm distributed under GPLv2 terms. Full license text "
                          "see in LICENSE file</p>\n"
                          "<p>Site: <a href=http://htrd.su>http://htrd.su</a></p>\n"
                          "<p>Copyright %1 Alexander 'hatred' Drozdov</p>"
                          "<p><strong>Authors:</strong></p>"
                          "<p>Alexander Drozdov - idea, programming</p>")
                       .arg(date_text));
}

void TaskTimerApp::onReset()
{
    int ret = QMessageBox::question(0,
                                    tr("Time Reset"),
                                    tr("Do you really want reset spent time for all tasks?"),
                                    QMessageBox::Yes,
                                    QMessageBox::No);
    if (ret == QMessageBox::Yes)
    {
        bool timerWasActive = m_timer->isActive();
        timerStop();

        QHash<QString, TaskInfo>::iterator it = m_taskInfo.begin();

        for (; it != m_taskInfo.end(); ++it)
        {
            const QString &id   = it.key();
            TaskInfo      &info = it.value();

            // Skip holded tasks
            if (info.hold)
                continue;

            info.time = 0;
            info.timeList.clear();
            updateMenuItemTime(id, info);

            // Update settings
            Settings s;
            s->setValue("/tasks/" + id + "/time",  info.time);
            s->setValue("/tasks/" + id + "/track_times", QVariant::fromValue(info.timeList));
        }

        if (timerWasActive)
            timerStart();
    }
}

void TaskTimerApp::onSettings()
{
    Settings       settings;
    SettingsDialog dlg(settings);

    if (dlg.exec() == QDialog::Accepted)
    {
        m_idle.setTimeout(settings->value("/core/timeout", 0).toInt() * 60);
        m_idle.start();
    }
}

void TaskTimerApp::onTimer()
{
    if (m_currentTaskId.isEmpty())
        return;

    if (m_timeAdjustDialog)
        m_currentTaskAdjust++;

    QString id = m_currentTaskId;

    if (m_taskInfo.contains(id))
    {
        TaskInfo &info = m_taskInfo[id];
        info.time++;
        const QDate date = QDate::currentDate();
        info.timeList[date].logged = false;
        info.timeList[date].time++;

        updateMenuItemTime(id, info);

        // Update settings
        Settings s;
        s->setValue("/tasks/" + id + "/time",  info.time);
        s->setValue("/tasks/" + id + "/track_times", QVariant::fromValue(info.timeList));
    }
}

void TaskTimerApp::onDayTimer()
{
    if (m_prevDate != QDate::currentDate())
    {
        m_prevDate = QDate::currentDate();

        // Update task display timers
        for (const auto& info : m_taskInfo)
        {
            updateMenuItemTime(info.id, info);
        }
    }
}

void TaskTimerApp::onUserIdle()
{
    qDebug() << "User idle";
    if (m_currentTaskId.isEmpty() || m_timerState != TaskTimerApp::TimerActive)
        return;

    if (m_timeAdjustDialog)
        return;

    {
        Settings s;
        m_currentTaskAdjust = s->value("/core/timeout", 0).toInt();
    }
    auto idleBeginDate = QDate::currentDate();

    m_timeAdjustDialog.reset(new QMessageBox());
    m_timeAdjustDialog->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    m_timeAdjustDialog->setDefaultButton(QMessageBox::Yes);
    m_timeAdjustDialog->setIcon(QMessageBox::Question);
    m_timeAdjustDialog->setWindowTitle(tr("Task Timer"));
    m_timeAdjustDialog->setText(tr("User inactive for a long time."));
    m_timeAdjustDialog->setInformativeText(tr("Adjust current task time for inactive period?"));

    auto ret = m_timeAdjustDialog->exec();
    switch (ret)
    {
        case QMessageBox::Yes:
        {
            // FIXME: make common with onTimer();
            QString id = m_currentTaskId;
            auto time = m_currentTaskAdjust;

            if (m_taskInfo.contains(id))
            {
                TaskInfo &info = m_taskInfo[id];
                info.time -= time;

                QDate date = QDate::currentDate();

                while (date != idleBeginDate)
                {
                    time -= info.timeList[date].time;
                    info.timeList.remove(date);
                    date = date.addDays(-1);
                }

                info.timeList[date].logged = false;
                info.timeList[date].time -= time;

                updateMenuItemTime(id, info);

                // Update settings
                Settings s;
                s->setValue("/tasks/" + id + "/time",  info.time);
                s->setValue("/tasks/" + id + "/track_times", QVariant::fromValue(info.timeList));
            }
            break;
        }
        default:
            break;
    }

    m_timeAdjustDialog.reset();
}

void TaskTimerApp::onUserActive()
{
    qDebug() << "User active";
}

void TaskTimerApp::onPauseResume()
{
    if (m_timer->isActive())
    {
        timerStop();

        if (!m_currentTaskId.isEmpty())
        {
            updateMenuItemState(m_currentTaskId, TimerPause);
        }
    }
    else
    {
        timerStart();

        if (!m_currentTaskId.isEmpty())
        {
            updateMenuItemState(m_currentTaskId, TimerActive);
        }
    }
}

void TaskTimerApp::onTaskAdd()
{
    addEditTask();
}

void TaskTimerApp::onHide()
{
    m_mainMenu->close();
    m_taskMenu->close();

    showMessage("Hidded");
}

