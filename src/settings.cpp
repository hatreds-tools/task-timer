/**
    @file
    This file is a part of "Task Timer" project\n
    http://htrd.su

    @brief
    Simple application settings wrapper implementation.

    @copyright 2010-2013 by Alexander Drozdov <adrozdoff@gmail.com>

    @par License
    This program is free software; you can redistribute it and/or modify
    it under the terms of the version 2 of GNU General Public License as
    published by the Free Software Foundation.\n

    For more information see LICENSE and LICENSE.ru files
*/


#include <QApplication>

#include "settings.h"

Settings::Settings()
    : m_settings(QSettings::IniFormat,
                QSettings::UserScope,
                QApplication::organizationName(),
                QApplication::applicationName())
{
}

QSettings *Settings::operator->()
{
    return &m_settings;
}

Settings::operator QSettings &()
{
    return m_settings;
}
